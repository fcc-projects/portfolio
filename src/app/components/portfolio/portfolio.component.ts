import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {
  projects = [
    {
      title: 'Tribute Page',
      subtitle: 'Project created for FCC speedrun',
      image: 'http://via.placeholder.com/350x150',
      description: 'A Sample Project - implemented in Angular 4.',
      url: '#'
    },
    {
      title: 'Tribute Page',
      subtitle: 'Project created for FCC speedrun',
      image: 'http://via.placeholder.com/350x150',
      description: 'A Sample Project - implemented in Angular 4.',
      url: '#'
    },
    {
      title: 'Tribute Page',
      subtitle: 'Project created for FCC speedrun',
      image: 'http://via.placeholder.com/350x150',
      description: 'A Sample Project - implemented in Angular 4.',
      url: '#'
    },
    {
      title: 'Tribute Page',
      subtitle: 'Project created for FCC speedrun',
      image: 'http://via.placeholder.com/350x150',
      description: 'A Sample Project - implemented in Angular 4.',
      url: '#'
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
